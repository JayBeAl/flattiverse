﻿
using Flattiverse;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ValeriusFighter
{

   
    public class Controller
    {

        //-----------------VARS------------------------------

        Connector connector;
        Boolean running = false;
        UniverseGroup universeGroup;
        Ship ship;
        FlattiverseMessage message;

        public delegate void FlattiverseChanged();
        public event FlattiverseChanged NewMessageEvent;
        List<FlattiverseMessage> messages = new List<FlattiverseMessage>();
        ReaderWriterLock messageLock = new ReaderWriterLock();

        float scanAngle=0;
        Map map = new Map();
        public event FlattiverseChanged NewScanEvent;

        float xImpulse, yImpulse;
        float direction;

        public bool ShipReady
        {
            get { if (ship != null) { return true; } else { return false; } }
        }

        public float ShipRadius
        {
            get { return ship.Radius; }
        }

        public List<Unit> Units
        {
            get
            {
                return map.Units;
            }
        }



        //------------------FUNCTIONS-----------------------

        public void Connect()
        {
            connector = new Connector("ludwig.berkenheier@stud.hs-esslingen.de", "124816");
        }

        public void Disconnect()
        {
            running = false;
        }

        public String ListUniverses()
        {
            String str = "";
            foreach (UniverseGroup ug in connector.UniverseGroups)
            {
                str += "Name:           " + ug.Name + "\n";
                str += "Description:    " + ug.Description + "\n";
                str += "Difficulty:     " + ug.Difficulty + "\n";
                str += "MaxShips:       " + ug.MaxShipsPerPlayer + "\n";
                foreach (Universe universe in ug.Universes)
                {
                    str += "Universe:       " + universe.Name + "\n";
                    foreach (Team team in ug.Teams)
                    {
                        str += "Team:           " + team.Name + "\n";
                    }
                }
                str += "\n\n";
            }
            return str;
        }

        public void Enter(string universeGroupName, string teamName)
        {
            universeGroup = connector.UniverseGroups[universeGroupName];
            Team team = universeGroup.Teams[teamName];
            universeGroup.Join("VirAlu", team);
            ship = universeGroup.RegisterShip("ValeriusFighter", "VirAluTry");

            Thread thread = new Thread(Run);
            thread.Name = "MainLoop";
            thread.Start();
        }

        public void Run()
        {
            ship.Continue();
            running = true;
            UniverseGroupFlowControl flowControl = universeGroup.GetNewFlowControl();
            FlattiverseMessage message;
            bool messagesReceived = false;
            messageLock.AcquireWriterLock(100);

            while (running)
            {
                
                while (connector.NextMessage(out message))
                {
                    messages.Add(message);
                    messagesReceived = true;
                }
                messageLock.ReleaseWriterLock();

                if (messagesReceived && NewMessageEvent != null)
                    NewMessageEvent();

                List<Unit> scannedUnits = ship.Scan(scanAngle, ship.ScannerArea.Limit);

                map.Insert(scannedUnits);
                if (NewScanEvent != null)
                    NewScanEvent();

                flowControl.Commit();
                flowControl.Wait();
            }
            connector.Close();
        }

        public List<FlattiverseMessage> Messages
        {
            get
            {
                messageLock.AcquireReaderLock(100);
                List<FlattiverseMessage> listCopy
                    = new List<FlattiverseMessage>(messages);
                messageLock.ReleaseReaderLock();
                return listCopy;
            }
        }

        public void Scan()
        {
            while (scanAngle <= 380)
            {
                List<Unit> scannedUnits = ship.Scan(scanAngle, ship.ScannerArea.Limit);
                scanAngle += ship.ScannerDegreePerScan - 2;
                
            }

        }

        public void Move()
        {
            float direction;
            if (xImpulse > 0)
            {
                if (yImpulse > 0)
                    direction = 315;
                else if (yImpulse < 0)
                    direction = 45;
                else
                    direction = 0;
            }
            else if (xImpulse < 0)
            {
                if (yImpulse > 0)
                    direction = 225;
                else if (yImpulse < 0)
                    direction = 135;
                else
                    direction = 0;
            }
            else
            {
                if (yImpulse > 0)
                    direction = 270;
                else if (yImpulse < 0)
                    direction = 90;
                else
                    direction = 0;
            }

            if (xImpulse != 0 || yImpulse != 0)
            {
               Vector acceleration = Vector.FromAngleLength(direction,ship.EngineAcceleration.Limit);
               ship.Move(acceleration);
            }

            xImpulse = 0;
            yImpulse = 0;
        }


        public void Impulse(int x, int y)
        {
            xImpulse += x;
            yImpulse += y;
        }

    }
}