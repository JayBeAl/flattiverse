﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ValeriusFighter.View;


namespace ValeriusFighter
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (Login())
            {
                Controller controller = new Controller();
                Application.Run(new View(controller));
            }
        }

        private static bool Login()
        {
            ValeriusFighter.View.LoginForm loginForm = new ValeriusFighter.View.LoginForm
            {
                Email = "xx";
                Password = "yy";
            };

        DialogResult dialogResult = loginForm.ShowDialog();


        }
    }
}
