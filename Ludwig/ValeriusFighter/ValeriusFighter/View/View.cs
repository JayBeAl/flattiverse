﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flattiverse;
using System.Drawing;


namespace ValeriusFighter
{
    public partial class View : Form
    {
        public Controller controller;
        List<Unit> units = new List<Unit>();

        public void View_Shown(object sender, EventArgs e)
        {
            controller.Connect();
           // textBoxMessages.Text = controller.ListUniverses();
            controller.Enter("Time Master", "None");
            controller.NewMessageEvent += NewMessage;
            radarScreen.Paint += RadarScreenPaintEventHandler;
            radarScreen.Resize += RadarScreenResizedHandler;
            controller.NewScanEvent += NewScan;
        }

        public View(Controller controller)
        {
            this.controller = controller;
            InitializeComponent();
            Shown += View_Shown;
          
        }

        private void View_FormClosingEventHandler(object sender, FormClosingEventArgs e)
        {
            controller.Disconnect();
        }

        private void NewMessage()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(NewMessage));
            }
            else
            {
                List<FlattiverseMessage> messages = controller.Messages;
                List<string> lines = new List<string>();
                foreach (FlattiverseMessage message in messages)
                    lines.Add(message.ToString());
                textBoxMessages.Lines = lines.ToArray();
                textBoxMessages.SelectionStart = textBoxMessages.Text.Length;
                textBoxMessages.ScrollToCaret();
                textBoxMessages.Refresh();
            }
        }

        private void RadarScreenPaintEventHandler(object sender, PaintEventArgs e)
        {
            if (!controller.ShipReady)
                return;

            // Compute magnification factor
            int radarScreenMinDimension = Math.Min(radarScreen.Width, radarScreen.Height);
            // Make screen at least 2000x2000 flattiverse miles with Ship at center
            // i.e. minimum screenPixels corresponds to 2000 flattiverse miles
            float displayFactor = radarScreenMinDimension / 2000f;

            float centerX = radarScreen.Width / 2;
            float centerY = radarScreen.Height / 2;

            float shipRadius = controller.ShipRadius * displayFactor;

            Graphics g = e.Graphics;
            g.DrawEllipse(Pens.White,
                centerX* - shipRadius, centerY - shipRadius,
                shipRadius * 2, shipRadius * 2);
            
            foreach (Unit u in units)
            {
                float uX = centerX + u.Position.X * displayFactor;
                float uY = centerY + u.Position.Y * displayFactor;
                float uR = u.Radius * displayFactor;
                g.DrawEllipse(Pens.Green, uX - uR, uY - uR, uR * 2, uR * 2);
            }


        }

        private void RadarScreenResizedHandler(object sender, EventArgs e)
        {
            radarScreen.Refresh();
        }

        private void NewScan()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(NewScan));
            }
            else
            {
                units = controller.Units;
                radarScreen.Invalidate();
            }
        }



    }
}
