﻿using Flattiverse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VirAluFighter.Model;
using VirAluFighter.View;

namespace VirAluFighter.Controller
{
    class Controller
    {

        private static Controller instance;
        private Connector connector;
        private bool isRunning;

        public Model.Model Model { get; set; }

        public static Controller GetInstance()
        {
            return instance ?? (instance = new Controller());
        }

        private Controller(){ }

        public void Start()
        {
            new Thread(MainLoop).Start();
        }

        public void MainLoop()
        {
            while(isRunning)
            {

            }
        }

        public void Stop()
        {
            isRunning = false;
        }

        #region connection
        public bool Connect()
        {
            while (!TryConnect(Model.Email, Model.Password))
            {
                LoginForm loginForm = new LoginForm
                {
                    Email = Model.Email,
                    Passwort = Model.Password
                };

                DialogResult dialogResult = loginForm.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    return false;
                    Model.Email = loginForm.Email;
                    Model.Password = loginForm.Passwort;
                }
            }
            return true;
        }

        public bool TryConnect(string userEmail, string password)
        {
            try
            {
                connector = new Connector(userEmail, password);
            }
            catch (GameException e)
            {
                if (e.ErrorNumber == 2)
                {
                    return false;
                }
                throw;
            }
            return true;
        }

        public void Disconnect()
        {
            connector.Close();
        }

        #endregion Connection

    }
}
