﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VirAluFighter.View;


namespace VirAluFighter.Controller
{
    static class Program
    {

        private static Model.Model model;
        private static Controller controller;

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]



        static void Main()
        {
            model = Model.Model.GetInstance();
            controller = Controller.GetInstance();
            controller.Model = model;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (controller.Connect())
            {
                Application.Run(new Gui(model,controller));
                controller.Disconnect();
            }
        }
    }
}

