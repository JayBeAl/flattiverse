﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VirAluFighter.View
{
    public partial class tes : Form
    {
        Model.Model model;
        Controller.Controller controller;
        

        public tes(Model.Model model,Controller.Controller controller)
        {
            this.model = model;
            this.controller = controller;
            InitializeComponent();
            FormClosing += Gui_FormClosing;

            controller.Start();

        }

        private void Gui_FormClosing(object sender, FormClosingEventArgs e)
        {
            controller.Stop();
        }
    }
}
