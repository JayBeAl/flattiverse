﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VirAluFighter.View
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        public String Email
        {
            get => tbEmail.Text ;
            set => tbEmail.Text = value;
        }

        public String Passwort
        {
            get => tbPasswort.Text;
            set => tbPasswort.Text = value;
        }

    }
}
