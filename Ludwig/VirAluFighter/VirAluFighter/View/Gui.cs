﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VirAluFighter
{
    public partial class Gui : Form
    {
        private Model.Model model;
        private readonly Controller.Controller controller;

        public Gui(Model.Model model, Controller.Controller controller)
        {
            this.model = model;
            this.controller = controller;
            InitializeComponent();
            FormClosing += Gui_FormClosing;
            controller.Start();
        }

        private void Gui_FormClosing(object sender, FormClosingEventArgs e)
        {
            controller.Stop();
        }
    }
}
