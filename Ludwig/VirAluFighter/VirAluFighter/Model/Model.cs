﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flattiverse;

namespace VirAluFighter.Model
{
    class Model
    {
        private static Model instance;

        private Credentials credentials = new Credentials();


        private Connector connector;

        public static Model GetInstance()
        {
            return instance ?? (instance = new Model());
        }

        private Model() { }

        
        
        public void Disconnect()
        {
            connector.Close();
        }
        public string Email
        {
            get => credentials.GetCredential(0);
            set => credentials.setCredential(0,value);
        }

        public string Password
        {
            get => credentials.GetCredential(1);
            set => credentials.setCredential(1,value);
        }


    }
}
