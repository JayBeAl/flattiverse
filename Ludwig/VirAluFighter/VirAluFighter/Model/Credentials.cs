﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirAluFighter.Model
{
    public class Credentials
    {
        private static string credentialsPath = "credentials.txt";
        private static string[] credentials;


        #region credentials
        public string GetCredential(int index)
        {
            ReadCredentials();
            return credentials[index];
        }

        public void setCredential(int index, string value)
        {
            ReadCredentials();
            if (credentials[index] != value)
            {
                credentials[index] = value;
                WriteCredentials();
            }
        }

        private static void ReadCredentials()
        {
            if (credentials == null)
            {
                credentials = File.Exists(credentialsPath) ? File.ReadAllLines(credentialsPath) : new[] { "", "" };
            }
        }

        private static void WriteCredentials()
        {
            File.WriteAllLines(credentialsPath, credentials);
        }

        #endregion 

    }
}
