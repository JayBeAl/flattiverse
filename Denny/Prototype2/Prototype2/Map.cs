﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flattiverse;
using System.Threading;

namespace Prototype2
{
    class Map
    {
        //>>Listen<<
        Dictionary<string, Unit> mapUnits = new Dictionary<string, Unit>();     //Dictionary -> Wert/Werte werden Tag/Namen zugeordnet

        ////Leseschreibschutz-Objekt für Map-Units
        ReaderWriterLock listLock = new ReaderWriterLock();

        //>>Properties<<
        public List<Unit> Units
        {
            get
            {
                listLock.AcquireReaderLock(100);
                List<Unit> units = new List<Unit>(mapUnits.Values);
                listLock.ReleaseReaderLock();
                return units;
            }
        }

        //>>Methoden<<
        public void Insert(List<Unit> units)
        {
            listLock.AcquireWriterLock(100);
            foreach(Unit unit in units)
            {
                mapUnits[unit.Name] = unit;
            }
            listLock.ReleaseWriterLock();
        }
    }
}
