﻿using Flattiverse;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace Prototype2
{
    public class Controller
    {
        //Logindaten
        string loginName = "";
        string loginPassword = "";
        //>>Variablen<<
        bool running = true;
        int xImpulse = 0;
        int yImpulse = 0;
        float scanAngle = 0;

        //>>Objekte<< aus Connector
        Connector connector;        
        Ship ship;
        UniverseGroup universeGroup;
        //>>Objekte<< aus eigenen Klassen
        Map map = new Map();

        //>>Events<<
        public delegate void FlattiverseChanged();          //Flattiverseevents aktivieren
        public event FlattiverseChanged NewMessageEvent;    //Event für Chataktualisierung
        public event FlattiverseChanged NewScanEvent;       //Event für Scanwiederholung

        //>>Listen<<
        List<FlattiverseMessage> messages = new List<FlattiverseMessage>();

        //Leseschreibschutz-Objekte für Chatnachrichten und Scans
        ReaderWriterLock messageLock = new ReaderWriterLock();
        ReaderWriterLock scanLock = new ReaderWriterLock();

        //>>Properties<<
        public List<FlattiverseMessage> Messages
        {
            get
            {
                messageLock.AcquireReaderLock(100);
                List<FlattiverseMessage> listCopy = new List<FlattiverseMessage>(messages);
                messageLock.ReleaseReaderLock();
                return listCopy;
            }
        }   
        public List<Unit> Units
        {
            get { return map.Units; }
        }
        public bool ShipReady
        {
            get { if (ship != null) { return true; } else { return false; } }
        }
        public float ShipRadius
            {
                get { return ship.Radius; }
            }

        //>>Methoden<<
        //für Verbindungshandling
        public void Connect()
        {
            connector = new Connector(loginName, loginPassword);
        }
        public void Disconnect()
        {
            running = false;
        }
        public void Enter(string universeGroupName, string teamName)
        {
            universeGroup = connector.UniverseGroups[universeGroupName];    //Anmelden ans gewünschte Universum
            Team team = universeGroup.Teams[teamName];                      //Eintragen in gewünschtes Team
            universeGroup.Join("JayBeAl", team);
            ship = universeGroup.RegisterShip("Destroyer", "Stardestroya"); //Raumschiff mit gewünschter Klasse registrieren und benennen
            Thread thread = new Thread(Run);                                //Thread für Hauptroutinenhandling erstellen
            thread.Name = "MainLoop";                                       //Benennen
            thread.Start();                                                 //Starten
        }

        //für Chathandling
        public void getMessages()
        {
            FlattiverseMessage flattiverseMessage;
            bool messagesRecieved = false;
            messageLock.AcquireWriterLock(100);
            while (connector.NextMessage(out flattiverseMessage))   //Bedingung: Solange es eine neue Message gibt
            {
                messages.Add(flattiverseMessage);                   //Message vom Typ Flattiversemessage der Liste hinzufügen
                messagesRecieved = true;
            }
            messageLock.ReleaseWriterLock();

            if (messagesRecieved && NewMessageEvent != null)        //Bei Bedingungserfüllung Event für Chat aufrufen
                NewMessageEvent();
        }

        //für Funktionen
        public void Scan()
        {
            List<Unit> scannedUnits = ship.Scan(scanAngle, ship.ScannerArea.Limit);     //Eigentlicher Scanbefehl mit Winkel und Reichweite
            scanLock.AcquireWriterLock(100);
            scanAngle += (ship.ScannerDegreePerScan - 5);
            map.Insert(scannedUnits);
            scanLock.ReleaseWriterLock();
            if (NewScanEvent != null)                                                   //Eventaufruf für Scans
                NewScanEvent();
        }
        public void Impulse(int x, int y)
        {
            xImpulse += x;
            yImpulse += y;
        }
        public void Move()
        {
            float direction;
            if (xImpulse > 0)
            {
                if (yImpulse > 0)
                    direction = 315;
                else if (yImpulse < 0)
                    direction = 45;
                else
                    direction = 0;
            }
            else if (xImpulse < 0)
            {
                if (yImpulse > 0)
                    direction = 225;
                else if (yImpulse < 0)
                    direction = 135;
                else
                    direction = 180;
            }
            else
            {
                if (yImpulse > 0)
                    direction = 270;
                else
                    direction = 90;
            }
            if (xImpulse != 0 || yImpulse != 0)
            {
                Vector acceleration = Vector.FromAngleLength(direction, ship.EngineAcceleration.Limit);
                ship.Move(acceleration);
            }
        }

        //Hauptroutine
        private void Run()
        {
            ship.Continue();        //Schiff spawnen lasssen. Denn registrieren heißt nicht gleich, dass es "lebt"
            running = true;
            using (UniverseGroupFlowControl universeGroupFlowControl = universeGroup.GetNewFlowControl())   //Wird für den Routineablauf benötigt, da ein vorgegebenes Ablaufmuster der Kommunikation zwischen Server und Client besteht.
                while(running)
                {
                    Scan();
                    Move();
                    getMessages();
                    universeGroupFlowControl.Commit();      //Übergaben der Paramter an den Server bei jedem Tick
                }
            connector.Close();
        }




        
        //Abrufen der verschiedenen Universen inklusive Typen, Schwierigkeiten, Maximale Leben etc
        //public string ListUniverses()
        //{
        //    string str = "";
        //    foreach (UniverseGroup universeGroup in connector.UniverseGroups)
        //    {
        //       str += "-> " + universeGroup.Name + " - " + universeGroup.Description + " - " + universeGroup.Difficulty + " - " + universeGroup.MaxShipsPerPlayer + "\n";
        //        foreach (Universe universe in universeGroup.Universes)
        //            str += "-----> Universe: " + universe.Name + "\n";
        //        foreach (Team team in universeGroup.Teams)
        //            str += "-----> Team: " + team.Name + "\n";4
        //    }
        //    return str;
        //}

    }
}