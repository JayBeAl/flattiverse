﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flattiverse;

namespace Prototype2
{
    public partial class View : Form
    {
        //>>Variablen<<
        int radarSize = 1;      //Radargröße von 1-3
        //>>Objekte<<
        Controller controller;

        //>>Listen<<
        List<Unit> units = new List<Unit>();

        //>>Konstruktor<<
        public View(Controller controller)
        {
            WindowState = FormWindowState.Maximized;            //Fullscreen
            this.controller = controller;
            InitializeComponent();

            Shown += View_Shown;                                //Abonnierung des View_Shown Events
            controller.NewMessageEvent += NewMessage;           //Abonnierung des Chat Events
            radarScreen.Resize += RadarScreenResizedHandler;    //Aboonierung des Events, dass den Radarscreen der Fenstergröße anpasst - > Skalierung
        }
        //>>Events<<
        private void RadarScreenResizedHandler(object sender, EventArgs e)
        {
            radarScreen.Refresh();
        }
        private void View_Shown(object sender, EventArgs e) //Initialisierung
        {
            controller.Connect();
            controller.Enter("Time Master", "None");
            radarScreen.Paint += RadarScreenPaintEventHandler;      //Abonnierung an das Draw-Event
            controller.NewScanEvent += NewScan;                     //Abbonierung an das Scan Event
        }
        private void NewScan()
        {
            if(InvokeRequired)                              //Thread der Controller Hautproutine gibt Aufgabe an den Thread der Windowsforms ab. Performancegrund
            {
                Invoke(new MethodInvoker(NewScan));
            }
            else
            {
                units = controller.Units;
                radarScreen.Invalidate();
            }
        }
        private void RadarScreenPaintEventHandler(object sender, PaintEventArgs e)
        {
            if (!controller.ShipReady)
                return;

            //Compute magnification factor
            int radarScreenMinDimension = Math.Min(radarScreen.Width, radarScreen.Height);
            //Make screen at least 2000x2000 flattiverse miles with Ship at center
            //i.e. minimum screenPixels corresponds to 2000 flattiverse miles
            float displayFactor = radarScreenMinDimension / (radarSize*1000f);

            float centerX = radarScreen.Width / 2;
            float centerY = radarScreen.Height / 2;

            float shipRadius = controller.ShipRadius * displayFactor;

            Graphics g = e.Graphics;
            g.DrawEllipse(Pens.White, centerX - shipRadius, centerY - shipRadius, shipRadius * 2, shipRadius * 2);

            foreach(Unit unit in units)
            {
                float uX = centerX + unit.Position.X * displayFactor;
                float uY = centerY + unit.Position.Y * displayFactor;
                float uR = unit.Radius * displayFactor;
                g.DrawEllipse(Pens.Green, uX - uR, uY - uR, uR * 2, uR * 2);        //DrawEllipse(Farbe,xpos,ypos,breite,höhe
            }
        }
        private void NewMessage()
        {
            if (InvokeRequired)                             //Thread der Controller Hautproutine gibt Aufgabe an den Thread der Windowsforms ab. Performancegrund
            {
                Invoke(new MethodInvoker(NewMessage));
            }
            else
            {
                List<FlattiverseMessage> messages = controller.Messages;
                List<string> lines = new List<string>();
                foreach (FlattiverseMessage flattiverseMessage in messages)
                {
                    lines.Add(flattiverseMessage.ToString());
                }
                chat.Lines = lines.ToArray();
                chat.SelectionStart = chat.Text.Length;
                chat.ScrollToCaret();                           //Bis an unteren Rand scrollen
                chat.Refresh();
            }
        }
        private void KeyDownEventHandler(object sender, KeyEventArgs e)
    {
            switch (e.KeyCode)
            {
                case Keys.W:
                    controller.Impulse(0, 1);
                    break;
                case Keys.A:
                    controller.Impulse(-1, 0);
                    break;
                case Keys.S:
                    controller.Impulse(0, -1);
                    break;
                case Keys.D:
                    controller.Impulse(1, 0);
                    break;
            }
            e.SuppressKeyPress = true;          //Abschalten des Tastentons
        }
        //>>Methoden<<
        private void View_FormClosing(object sender, FormClosingEventArgs e)
        {
            controller.Disconnect();
        }
    }
}
