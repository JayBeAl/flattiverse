﻿namespace Flattiverse.Client
{
    partial class View
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.chat = new System.Windows.Forms.RichTextBox();
            this.radarScreen = new System.Windows.Forms.Panel();
            this.energyValue = new System.Windows.Forms.Label();
            this.energyBar = new System.Windows.Forms.ProgressBar();
            this.speedValue = new System.Windows.Forms.Label();
            this.impulseValues = new System.Windows.Forms.Label();
            this.engineAccel = new System.Windows.Forms.Label();
            this.energyDrain = new System.Windows.Forms.Label();
            this.radarScreen.SuspendLayout();
            this.SuspendLayout();
            // 
            // chat
            // 
            this.chat.Location = new System.Drawing.Point(0, 0);
            this.chat.Name = "chat";
            this.chat.ReadOnly = true;
            this.chat.Size = new System.Drawing.Size(238, 388);
            this.chat.TabIndex = 0;
            this.chat.Text = "";
            // 
            // radarScreen
            // 
            this.radarScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radarScreen.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radarScreen.Controls.Add(this.energyValue);
            this.radarScreen.Controls.Add(this.energyBar);
            this.radarScreen.Location = new System.Drawing.Point(244, 12);
            this.radarScreen.Name = "radarScreen";
            this.radarScreen.Size = new System.Drawing.Size(544, 426);
            this.radarScreen.TabIndex = 1;
            // 
            // energyValue
            // 
            this.energyValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.energyValue.AutoSize = true;
            this.energyValue.BackColor = System.Drawing.Color.Lime;
            this.energyValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.energyValue.ForeColor = System.Drawing.Color.Coral;
            this.energyValue.Location = new System.Drawing.Point(39, 384);
            this.energyValue.Name = "energyValue";
            this.energyValue.Size = new System.Drawing.Size(0, 29);
            this.energyValue.TabIndex = 3;
            // 
            // energyBar
            // 
            this.energyBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.energyBar.Location = new System.Drawing.Point(3, 374);
            this.energyBar.Name = "energyBar";
            this.energyBar.Size = new System.Drawing.Size(200, 49);
            this.energyBar.TabIndex = 2;
            // 
            // speedValue
            // 
            this.speedValue.AutoSize = true;
            this.speedValue.Location = new System.Drawing.Point(73, 391);
            this.speedValue.Name = "speedValue";
            this.speedValue.Size = new System.Drawing.Size(35, 13);
            this.speedValue.TabIndex = 2;
            this.speedValue.Text = "label1";
            // 
            // impulseValues
            // 
            this.impulseValues.AutoSize = true;
            this.impulseValues.Location = new System.Drawing.Point(73, 407);
            this.impulseValues.Name = "impulseValues";
            this.impulseValues.Size = new System.Drawing.Size(35, 13);
            this.impulseValues.TabIndex = 3;
            this.impulseValues.Text = "label1";
            // 
            // engineAccel
            // 
            this.engineAccel.AutoSize = true;
            this.engineAccel.Location = new System.Drawing.Point(73, 420);
            this.engineAccel.Name = "engineAccel";
            this.engineAccel.Size = new System.Drawing.Size(35, 13);
            this.engineAccel.TabIndex = 4;
            this.engineAccel.Text = "label1";
            // 
            // energyDrain
            // 
            this.energyDrain.AutoSize = true;
            this.energyDrain.Location = new System.Drawing.Point(73, 433);
            this.energyDrain.Name = "energyDrain";
            this.energyDrain.Size = new System.Drawing.Size(35, 13);
            this.energyDrain.TabIndex = 5;
            this.energyDrain.Text = "label1";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.energyDrain);
            this.Controls.Add(this.engineAccel);
            this.Controls.Add(this.impulseValues);
            this.Controls.Add(this.speedValue);
            this.Controls.Add(this.radarScreen);
            this.Controls.Add(this.chat);
            this.KeyPreview = true;
            this.Name = "View";
            this.Text = "StarDestroya";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.View_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownEventHandler);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyUpEventHandler);
            this.radarScreen.ResumeLayout(false);
            this.radarScreen.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox chat;
        private System.Windows.Forms.Panel radarScreen;
        private System.Windows.Forms.ProgressBar energyBar;
        private System.Windows.Forms.Label energyValue;
        private System.Windows.Forms.Label speedValue;
        private System.Windows.Forms.Label impulseValues;
        private System.Windows.Forms.Label engineAccel;
        private System.Windows.Forms.Label energyDrain;
    }
}

