﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Flattiverse.Client
{
    public partial class View : Form
    {
        //>>Variablen<<
        const int radarSize = 1;      //Radargröße von 1-3 groß-klein
        //>>Objekte<<
        Controller controller;

        //>>Listen<<
        List<Unit> units = new List<Unit>();
        List<Pen> colour = new List<Pen>() { Pens.Yellow, Pens.Green, Pens.Blue, Pens.Lavender, Pens.Red };
        

        //>>Konstruktor<<
        public View(Controller controller)
        {
            WindowState = FormWindowState.Maximized;            //Fullscreen
            this.controller = controller;
            InitializeComponent();
            energyValue.BackColor = Color.Transparent;

            Shown += View_Shown;                                //Abonnierung des View_Shown Events
            controller.NewMessageEvent += NewMessage;           //Abonnierung des Chat Events
            radarScreen.Resize += RadarScreenResizedHandler;    //Aboonierung des Events, dass den Radarscreen der Fenstergröße anpasst - > Skalierung
        }
        //>>Events<<
        private void RadarScreenResizedHandler(object sender, EventArgs e)
        {
            radarScreen.Refresh();
        }
        private void View_Shown(object sender, EventArgs e) //Initialisierung
        {
            controller.Connect();
            controller.Enter("Time Master", "None");
            radarScreen.Paint += RadarScreenPaintEventHandler;      //Abonnierung an das Draw-Event
            controller.NewScanEvent += NewScan;                     //Abbonierung an das Scan Event
        }
        private void NewScan()
        {
            if(InvokeRequired)                              //Thread der Controller Hautproutine gibt Aufgabe an den Thread der Windowsforms ab. Performancegrund
            {
                Invoke(new MethodInvoker(NewScan));
            }
            else
            {
                units = controller.Units;
                radarScreen.Invalidate();
            }
        }
        private void RadarScreenPaintEventHandler(object sender, PaintEventArgs e)
        {
            if (!controller.ShipReady)
                return;

            //Compute magnification factor
            int radarScreenMinDimension = Math.Min(radarScreen.Width, radarScreen.Height);
            //Make screen at least 2000x2000 flattiverse miles with Ship at center
            //i.e. minimum screenPixels corresponds to 2000 flattiverse miles
            float displayFactor = radarScreenMinDimension / (radarSize * 1000f);

            float centerX = radarScreen.Width / 2;
            float centerY = radarScreen.Height / 2;

            float shipRadius = controller.ShipRadius * displayFactor;

            Graphics g = e.Graphics;
            g.DrawEllipse(Pens.White, centerX - shipRadius, centerY - shipRadius, shipRadius * 2, shipRadius * 2);

            foreach(Unit unit in units)
            {
                float uX = centerX + unit.Position.X * displayFactor;
                float uY = centerY + unit.Position.Y * displayFactor;
                float uR = unit.Radius * displayFactor;
                g.DrawEllipse(colour[controller.UnitTypeToInt(unit)], uX - uR, uY - uR, uR * 2, uR * 2);        //DrawEllipse(Farbe,xpos,ypos,breite,höhe)
                Font font = new Font("Arial", 16);
                SizeF textSize = g.MeasureString(unit.Name, font);
                if (unit.Kind == UnitKind.Sun)
                {
                    Sun sun = unit as Sun;
                    g.DrawEllipse(Pens.Orange, uX - sun.Coronas[0].Radius, uY - sun.Coronas[0].Radius, sun.Coronas[0].Radius * 2, sun.Coronas[0].Radius * 2);
                    g.DrawString("Gravityfaktor: " + unit.Gravity.ToString(), font, Brushes.White, uX - uR, uY + textSize.Height / 2);
                }                
                if(uR * 2 < textSize.Width)
                {
                    g.DrawString(unit.Name, font, Brushes.White, uX + uR, uY - textSize.Height/2);
                }
                else
                {
                    g.DrawString(unit.Name, font, Brushes.White, uX - textSize.Width/2, uY - textSize.Height/2);
                }

            }
            energyBar.Value = Convert.ToInt32(controller.ShipEnergy/controller.ShipEnergyMax*100);
            energyValue.Text = "" + controller.ShipEnergy.ToString() + "/" + Convert.ToInt32(controller.ShipEnergyMax).ToString();
            speedValue.Text = "" + controller.ShipSpeed.Length.ToString() + "    " + controller.ShipSpeed.Angle.ToString();
            impulseValues.Text = "" + controller.Impulse.ToString();
            energyDrain.Text = controller.EnergyDrain.ToString();
            
            
            
        }
        private void NewMessage()
        {
            if (InvokeRequired)                             //Thread der Controller Hautproutine gibt Aufgabe an den Thread der Windowsforms ab. Performancegrund
            {
                Invoke(new MethodInvoker(NewMessage));
            }
            else
            {
                List<FlattiverseMessage> messages = controller.Messages;
                List<string> lines = new List<string>();
                foreach (FlattiverseMessage flattiverseMessage in messages)
                {
                    lines.Add(flattiverseMessage.ToString());
                }
                chat.Lines = lines.ToArray();
                chat.SelectionStart = chat.Text.Length;
                chat.ScrollToCaret();                           //Bis an unteren Rand scrollen
                chat.Refresh();
            }
        }
        private void KeyDownEventHandler(object sender, KeyEventArgs e)
    {
            switch (e.KeyCode)
            {
                case Keys.I:
                    controller.Impulse = 1;
                    break;
                case Keys.K:
                    controller.Impulse = -1;
                    break;
                case Keys.W:
                    controller.SetDirection(0);
                    break;
                case Keys.A:
                    controller.SetDirection(1);
                    break;
                case Keys.S:
                    controller.SetDirection(2);
                    break;
                case Keys.D:
                    controller.SetDirection(3);
                    break;
            }
            e.SuppressKeyPress = true;          //Abschalten des Tastentons
        }
        private void KeyUpEventHandler(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    controller.UnsetDirection(0);
                    break;
                case Keys.A:
                    controller.UnsetDirection(1);
                    break;
                case Keys.S:
                    controller.UnsetDirection(2);
                    break;
                case Keys.D:
                    controller.UnsetDirection(3);
                    break;
            }
        }
        //>>Methoden<<
        private void View_FormClosing(object sender, FormClosingEventArgs e)
        {
            controller.Disconnect();
        }
    }
}
