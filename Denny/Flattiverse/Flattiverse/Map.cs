﻿using System.Collections.Generic;
using System.Threading;

namespace Flattiverse.Client
{
    class Map
    {
        //>> Variablen
        int tick = 0;
        //>>Listen<<
        Dictionary<string, Unit> mapUnits = new Dictionary<string, Unit>();     //Dictionary -> Wert/Werte werden Tag/Namen zugeordnet
        List<string> delete = new List<string>();

        ////Leseschreibschutz-Objekt für Map-Units
        ReaderWriterLock listLock = new ReaderWriterLock();

        //>>Properties<<
        public List<Unit> Units
        {
            get
            {
                listLock.AcquireReaderLock(100);
                List<Unit> units = new List<Unit>(mapUnits.Values);
                listLock.ReleaseReaderLock();
                return units;
            }
        }

        //>>Methoden<<
        public void Insert(List<Unit> units)
        {
            foreach(KeyValuePair<string,Unit> dictionary in mapUnits)
            {
                Tag t = dictionary.Value.Tag as Tag;
                if((tick - t.ScannedAt) == 6)
                {
                    delete.Add(dictionary.Key);
                }
            }
            listLock.AcquireWriterLock(100);
            foreach (string str in delete)
            {
                mapUnits.Remove(str);
            }
            foreach(Unit unit in units)
            {
                Tag tag = new Tag();
                tag.ScannedAt = tick;
                unit.Tag = tag;
                mapUnits[unit.Name] = unit;
            }
            listLock.ReleaseWriterLock();
            tick++;
            delete.Clear();
        }
    }
}
