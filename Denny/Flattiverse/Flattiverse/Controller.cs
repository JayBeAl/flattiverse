﻿using System.Threading;
using System.Collections.Generic;

namespace Flattiverse.Client
{
    public class Controller
    {
        //Logindaten
        string loginName = "degait00@hs-esslingen.de";
        string loginPassword = "Studimail2018_";
        //>>Variablen<<
        bool running = true;
        //int xImpulse = 0;
        //int yImpulse = 0;
        int impulse = 0;
        int accelLevels = 4;
        int direction = 0;
        float scanAngle = 0;
        float energyDrain = 0;
        float prevEnergy = 0;
        Vector firstUnitPrevPos;
        Vector actualSpeed = new Vector(0, 0);
        string firstUnitName = "";
        bool[] keys = new bool[4] {false,false,false,false};
        int tick = 0;

        //>>Objekte<< aus Connector
        Connector connector;
        Ship ship;
        UniverseGroup universeGroup;
        //>>Objekte<< aus eigenen Klassen
        Map map = new Map();

        //>>Events<<
        public delegate void FlattiverseChanged();          //Flattiverseevents aktivieren
        public event FlattiverseChanged NewMessageEvent;    //Event für Chataktualisierung
        public event FlattiverseChanged NewScanEvent;       //Event für Scanwiederholung

        //>>Listen<<
        List<FlattiverseMessage> messages = new List<FlattiverseMessage>();

        //Leseschreibschutz-Objekte für Chatnachrichten und Scans
        ReaderWriterLock messageLock = new ReaderWriterLock();
        ReaderWriterLock scanLock = new ReaderWriterLock();

        //>>Properties<<
        public List<FlattiverseMessage> Messages
        {
            get
            {
                messageLock.AcquireReaderLock(100);
                List<FlattiverseMessage> listCopy = new List<FlattiverseMessage>(messages);
                messageLock.ReleaseReaderLock();
                return listCopy;
            }
        }
        public List<Unit> Units
        {
            get { return map.Units; }
        }
        public bool ShipReady
        {
            get { if (ship != null) { return true; } else { return false; } }
        }
        public float ShipRadius
        {
            get { return ship.Radius; }
        }
        public float ShipEnergy
        {
            get { return ship.Energy; }
        }
        public float ShipEnergyMax
        {
            get { return ship.EnergyMax; }
        }
        public Vector ShipSpeed
        {
            get { return actualSpeed; }
        }
        //public int XImpulse
        //{ get { return xImpulse; } }
        //public int YImpulse
        //{ get { return yImpulse; } }
        public int Impulse
        {
            get { return impulse; }
            set
            {
                if(value == 1 && impulse < accelLevels || value == -1 && impulse > 0)
                impulse += value;
            }
        }
        public float EnergyDrain
        { get {   return energyDrain; } }


        //>>Methoden<<
        //für Verbindungshandling
        public void Connect()
        {
            connector = new Connector(loginName, loginPassword);
        }
        public void Disconnect()
        {
            running = false;
        }
        public void Enter(string universeGroupName, string teamName)
        {
            universeGroup = connector.UniverseGroups[universeGroupName];    //Anmelden ans gewünschte Universum
            Team team = universeGroup.Teams[teamName];                      //Eintragen in gewünschtes Team
            universeGroup.Join("JayBeAl", team);
            ship = universeGroup.RegisterShip("Destroyer", "Stardestroya"); //Raumschiff mit gewünschter Klasse registrieren und benennen
            Thread thread = new Thread(Run);                                //Thread für Hauptroutinenhandling erstellen
            thread.Name = "MainLoop";                                       //Benennen
            thread.Start();                                                 //Starten
        }
        
        //für Chathandling
        public void getMessages()
        {
            FlattiverseMessage flattiverseMessage;
            bool messagesRecieved = false;
            messageLock.AcquireWriterLock(100);
            while (connector.NextMessage(out flattiverseMessage))   //Bedingung: Solange es eine neue Message gibt
            {
                messages.Add(flattiverseMessage);                   //Message vom Typ Flattiversemessage der Liste hinzufügen
                messagesRecieved = true;
            }
            messageLock.ReleaseWriterLock();

            if (messagesRecieved && NewMessageEvent != null)        //Bei Bedingungserfüllung Event für Chat aufrufen
                NewMessageEvent();
        }

        //für Funktionen
        public void Scan()
        {
            if (tick % 3 == 0)
            {
                List<Unit> scannedUnits = ship.Scan(scanAngle, ship.ScannerArea.Limit);     //Eigentlicher Scanbefehl mit Winkel und Reichweite
                scanLock.AcquireWriterLock(100);
                scanAngle += (ship.ScannerDegreePerScan - 5);
                map.Insert(scannedUnits);
                scanLock.ReleaseWriterLock();
                if (NewScanEvent != null)                                                   //Eventaufruf für Scans
                    NewScanEvent();
            }
            tick++;
        }
        //public void Impulse(int x, int y)
        //{
        //    xImpulse += x;
        //    yImpulse += y;
        //}
        public void SetDirection(int index)
        {
            keys[index] = true;
        }
        public void UnsetDirection(int index)
        {
            keys[index] = false;
        }
        public void Move()
        {
            if (keys[0])                        //keys[0 = W, 1 = A, 2 = S, 3 = D]
            {
                if (keys[1])
                    direction = 225;
                else if (keys[3])
                    direction = 315;
                else
                    direction = 270;
            }
            else if (keys[1])
            {
                if (keys[2])
                    direction = 135;
                else
                    direction = 180;
            }
            else if (keys[2])
            {
                if (keys[3])
                    direction = 45;
                else
                    direction = 90;
            }
            else
            {
                direction = 0;
            }
            if (keys[0] || keys[1] || keys[2] || keys[3])
            {
                Vector acceleration = Vector.FromAngleLength(direction, impulse * (ship.EngineAcceleration.Limit / accelLevels));
                ship.Move(acceleration);
            }


            //ALTE STEUERUNG
            //float direction;
            //if (xImpulse > 0)
            //{
            //    if (yImpulse > 0)
            //        direction = 315;
            //    else if (yImpulse < 0)
            //        direction = 45;
            //    else
            //        direction = 0;
            //}
            //else if (xImpulse < 0)
            //{
            //    if (yImpulse > 0)
            //        direction = 225;
            //    else if (yImpulse < 0)
            //        direction = 135;
            //    else
            //        direction = 180;
            //}
            //else
            //{
            //    if (yImpulse > 0)
            //        direction = 270;
            //    else
            //        direction = 90;
            //}
            //if (xImpulse != 0 || yImpulse != 0)
            //{
            //    Vector acceleration = Vector.FromAngleLength(direction,impulse*(ship.EngineAcceleration.Limit/accelFactor));
            //    ship.Move(acceleration);
            //}
        }
        public int UnitTypeToInt(Unit unit)
        {
            switch (unit.Kind)
            {
                case UnitKind.Sun:
                    return 0;
                case UnitKind.Planet:
                    return 1;
                case UnitKind.Asteroid:
                    return 2;
                case UnitKind.PlayerShip:
                    return 3;
                case UnitKind.Buoy:
                    return 4;
                    //....
                    //....
                default:
                    return 4;
            }
        }

        public void ComputeSpeed()
        {
            Unit unit = map.Units[0];
            if(unit.Name != firstUnitName)
            {
                firstUnitName = unit.Name;
                firstUnitPrevPos = unit.Position;
            }
            else
            {
                actualSpeed = unit.Position - firstUnitPrevPos;
                firstUnitPrevPos = unit.Position;
            }
        }

        public void ComputeEnergyDrain()
        {
            energyDrain = prevEnergy - ship.Energy;
            prevEnergy = ship.Energy;
        }

        public void Functionroutine()
        {
            //ComputeSpeed();
            ComputeEnergyDrain();
        }
        //Hauptroutine
        private void Run()
        {
            ship.Continue();        //Schiff spawnen lasssen. Denn registrieren heißt nicht gleich, dass es "lebt"
            running = true;
            using (UniverseGroupFlowControl universeGroupFlowControl = universeGroup.GetNewFlowControl())   //Wird für den Routineablauf benötigt, da ein vorgegebenes Ablaufmuster der Kommunikation zwischen Server und Client besteht.
                while(running)
                {
                    Scan();
                    Move();
                    Functionroutine();
                    getMessages();
                    universeGroupFlowControl.Commit();      //Übergaben der Paramter an den Server bei jedem Tick
                }
            connector.Close();
        }





        //Abrufen der verschiedenen Universen inklusive Typen, Schwierigkeiten, Maximale Leben etc
        public string ListUniverses()
        {
            string str = "";
            foreach (UniverseGroup universeGroup in connector.UniverseGroups)
            {
                str += "-> " + universeGroup.Name + " - " + universeGroup.Description + " - " + universeGroup.Difficulty + " - " + universeGroup.MaxShipsPerPlayer + "\n";
                foreach (Universe universe in universeGroup.Universes)
                    str += "-----> Universe: " + universe.Name + "\n";
                foreach (Team team in universeGroup.Teams)
                    str += "-----> Team: " + team.Name + "\n";
            }
            return str;
        }

    }
}